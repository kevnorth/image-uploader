const express = require("express");
const { uid } = require('uid');
const unggah = require('unggah')
const cors = require('cors');
const bucketName = 'zumpul-images';

const port = 3001;
const app = express();
app.use(cors())


const upload = unggah({
  limits: {
    fileSize: 10e6 // in bytes
  },
  storage: unggah.gcs({
    keyFilename: './google-key.json',
    bucketName,
    rename: (req, file) => {
      return `${uid()}-${file.originalname}`  // this is the default
    }
  })
})

app.post("/images", upload.single('filename'), (req, res) => {
  console.log(req.file)
  res.status(201).json(req.file)
});


// Start the server
const server = app.listen(port, (error) => {
  if (error) return console.log(`Error: ${error}`);
  console.log(`Server listening on port ${server.address().port}`);
});
